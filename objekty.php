<?php

class uzivatel
{
    private $heslo;
    protected $jmeno;
    protected $email;
    protected $id_uzivatel;
    protected $database;
    
    public function __construct($PDO, $jm = NULL)
    {
      $this->database = $PDO;
      if($jm != NULL)
      {
        $sql = "SELECT * FROM uzivatel WHERE jmeno_uzivatel = '$jm' OR email_uzivatel = '$jm'";
        $vysledek = $this->database->query($sql)->fetch();
        $this->jmeno = $vysledek['jmeno_uzivatel']; 
        $this->heslo = $vysledek['heslo_uzivatel'];
        $this->email = $vysledek['email_uzivatel']; 
        $this->id_uzivatel = $vysledek['id_uzivatel'];
      } 
    }
    
    public function Prihlasit($jm, $he)
    {
      $he = sha1($he);
      
      if(($this->jmeno == $jm || $this->email == $jm) && $this->heslo == $he)
      {
        $_SESSION['name'] = "uzivatelLogged";
        $_SESSION['nick'] = $this->jmeno;
        $_SESSION['email'] = $this->email; 
        $_SESSION['id_uzivatel'] = $this->id_uzivatel;
        return "PRIHLASENO";
      }
      
      else
      {
        return "SPATNE UDAJE";
      }
    }
    
    public function Registrovat($jm, $he, $em)
    { 
      $he = sha1($he);
      $sql = "SELECT jmeno_uzivatel, email_uzivatel FROM uzivatel WHERE jmeno_uzivatel = '$jm' OR email_uzivatel = '$em'";    
      $vysledek = $this->database->query($sql)->fetch();
      
      if($vysledek == NULL)
      {
        $sql = "INSERT INTO uzivatel (jmeno_uzivatel, heslo_uzivatel, email_uzivatel) VALUES ('$jm','$he','$em')";
        $this->database->query($sql); 
        return "USPESNE ZAREGISTROVAN";
      }
      
      else if($vysledek['email_uzivatel'] == $em)
      {
        return "SHODA EMAILU, UCET NEZALOZEN";
      }
      
      else
      {
        return "SHODA NAZVU UCTU, UCET NEZALOZEN";
      }
    }
     
    public function ZapomenuteHesloSend($em)
    {
      $sql = "SELECT * FROM uzivatel WHERE email_uzivatel = '$em'";
      
      $vysledek = $this->database->query($sql)->fetch();
      
      if($vysledek!=NULL)
      {
        $this->jmeno = $vysledek['jmeno_uzivatel']; 
        $this->heslo = $vysledek['heslo_uzivatel'];
        $this->email = $vysledek['email_uzivatel']; 
        $sql = "UPDATE uzivatel SET email_potvrzeni=1 WHERE email_uzivatel='$em'";
        $this->database->query($sql);
        
        $cast1 = substr($this->heslo,0,10);
        $cast2 = substr($this->heslo,30,40);
        
        $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parse = parse_url($url);
        $ZPkod = $parse['host']; 
        $ZPkod .= ($parse['host'] == "localhost") ?"/religis/index.php?FPa=" : "/index.php?FPa=";
        $ZPkod .= $this->jmeno . "." .  $cast1 . $cast2;
        
        //echo $ZPkod . "<br>"; 
        
       $subject = 'Zapomenute Heslo - BLOG/Jan Retych';
        $headers = "To: ". $this->email ."\r\n". 
                   "From: Jan retych - blog\r\n". 
                   "Content-type: text/html; charset=UTF-8" . "\r\n"; 
        $zprava = '<html>
                    <head>
                    </head>
                    
                    <body style=" text-align: center; color: rgb(67,67,67); font-size: 15px;">
                      <p>Doslechli jsme se ze jste zapomnel heslo, </p>
                      <p>Kliknutim na tento odkaz si jej muzete obnovit </p>
                      <a href="'. $ZPkod .'" style="font-size: 25px; cursor: pointer;">Prihlasovaci jmeno: '. $this->jmeno .'</a>
                    </body>
                    </html>
                      ';
        
        mail($this->email, $subject, $zprava, $headers);
      }
      return "RESET HESLA BYL ZASLAN NA UVEDENY EMAIL, POKUD EMAIL EXISTUJE A JE V DATABAZI";
    }
    
    public function ZmenitHesloForm()
    {
      echo '
            <form method="post" id="ZmenitHeslo">
              <input type="password" name="NoveHeslo" id="NoveHeslo" placeholder="Nove heslo" required>
              <input type="password" name="NoveHesloZnova" id="NoveHesloZnova" placeholder="Nove heslo znova" required>
              <input type="submit" value="Změnit heslo" name="ZmenitHesloSubmit">        
            </form>';              
    }
    
    public function ZmenitHeslo($jm, $he) 
    {
      $he = sha1($he);
      $sql = "UPDATE uzivatel SET heslo_uzivatel='$he', email_potvrzeni = NULL WHERE jmeno_uzivatel='$jm'";
      $this->database->query($sql);
      return '<p>HESLO USPESNE POZMENENO </p> <a href="index.php">VRATIT SE NA UVODNI STRANKU</a>'; 
    }
    
    public function ZapomenuteHeslo($hash)
    {
      $jm = strtok($hash, '.');
      
      $sql = "SELECT * FROM uzivatel WHERE jmeno_uzivatel = '$jm' AND email_potvrzeni = 1";
      $vysledek = $this->database->query($sql)->fetch();
      
      if($vysledek != NULL)
      {
        $this->jmeno = $vysledek['jmeno_uzivatel']; 
        $this->heslo = $vysledek['heslo_uzivatel'];
        
        $cast1 = substr($this->heslo,0,10);
        $cast2 = substr($this->heslo,30,40);
        $kod = $this->jmeno . "." . $cast1 . $cast2;
        
        if($kod == $hash)
        { 
          if(!isset($_SESSION)) session_start();
          
          $_SESSION['nick'] = $this->jmeno;
          $this->ZmenitHesloForm();  
        }  
      }
    }    
}

class koment extends uzivatel
{
    protected $text;
    protected $id_koment;
    protected $IP;
    protected $datum;
    
    public function __construct($PDO)
    {
      $this->database = $PDO;
    }
    
    protected function vypiskomentu($cislo)
    {
      $sql = "SELECT * 
      FROM komentar inner join uzivatel using (id_uzivatel) 
      WHERE id_prispevek='$cislo'
      ORDER BY datum_koment DESC";
      $vysledek = $this->database->query($sql)->fetchAll();
      
      foreach ($vysledek as $row)
      {       
          $this->id_koment = $row['id_komentar'];
          $this->text = $row['text_koment'];  
          $this->id_uzivatel = $row['id_uzivatel'];
          $this->email = $row['email_uzivatel'];
          $this->jmeno = $row['jmeno_uzivatel'];
          $this->IP = $row['IP'];
          $this->datum = $row['datum_koment'];
                                                                                                                         // TISK
          echo '
              <div class="komenty" id="'. $this->id_koment .'">
              <h3>'. $this->datum .'</h3>
              <h3>'. $this->IP .'</h3>
              <a href="index.php?ZJ='. $this->id_uzivatel .'" title="Zobrazit uzivatel">'. $this->jmeno .'</a>
              <h1>- '. $this->email .'</h1>
                
              <p>'. $this->text .'</p>
              ';
          if(isset($_SESSION['name']))
          {    
            if($_SESSION['id_uzivatel'] == $this->id_uzivatel)
            {
                echo '
                <button type="button" class="edit">EDITOVAT</button>
                <button type="button" class="smazat">SMAZAT</button>
                <form class="EditKoment" method="post" class="'. $this->id_koment .'">
                  <textarea class="textEditKoment" rows="2">'. $this->text .'</textarea>
                  <input type="submit" value="EDITOVAT">
                </form>
                ';    
            }
          }
         echo '</div>';     
      }  
    }
    
    public function PocetKomentu($IDp)
    {
        $sql = "SELECT count(pocet_komentaru) FROM prispevek inner join komentar using(id_prispevek) WHERE id_prispevek='$IDp'";
        $vysledek = $this->database->query($sql)->fetch();
        $pocet = $vysledek['count(pocet_komentaru)'];
        $sql = "UPDATE prispevek SET pocet_komentaru='$pocet' WHERE id_prispevek='$IDp'";
        $this->database->query($sql);
    }
    
    public function vytvoritKoment($IDp, $IDu, $text)
    {
      $sql = "SELECT * FROM komentar WHERE text_koment='$text'";
      $vysledek = $this->database->query($sql)->fetch();
      
      if($vysledek == NULL)
      {  
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
        {
              $Ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
        {
              $Ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else $Ip = $_SERVER['REMOTE_ADDR'];    
         
        $sql = "INSERT INTO komentar 
        (id_prispevek, id_uzivatel, text_koment, IP) 
        VALUES ('$IDp','$IDu', '$text','$Ip')";
        
        $this->database->query($sql);
        
        $this->PocetKomentu($IDp);
      }
      else echo "<script> alert('NEMUZES ODESLAT VICEKRAT STEJNY PRISPEVEK!'); </script>";  
    }

    public function editKoment($text, $IDk)
    {
      $this->id_uzivatel = $_SESSION['id_uzivatel'];
      $sql = "SELECT id_uzivatel FROM komentar WHERE id_uzivatel='". $this->id_uzivatel ."'";
      $vysledek = $this->database->query($sql)->fetch(); 
      
      if($vysledek != NULL)
      {
        $sql = "UPDATE komentar SET text_koment='$text' WHERE id_komentar='$IDk'";
        $this->database->query($sql);    
      } 
    }
    
    public function smazatKoment($IDk, $IDp)
    {
      $this->id_uzivatel = $_SESSION['id_uzivatel'];
      $sql = "SELECT id_uzivatel FROM komentar WHERE id_uzivatel='". $this->id_uzivatel ."'";
      $vysledek = $this->database->query($sql)->fetch(); 
      
      if($vysledek != NULL)
      {
        $sql = "DELETE FROM komentar WHERE id_komentar = '$IDk'";
        $this->database->query($sql);    
      }
      $this->PocetKomentu($IDp);
    }
}

class prispevek extends koment
{
    private $src_obrazek;
    private $pocet_komentaru;
    private $nadpis; 
    protected $id_prispevek;
    
    public function __construct($PDO)
    {
      $this->database = $PDO;
    }
    
    private function vypisclanku($vysledek, $or)
    {
      foreach ($vysledek as $row)
      {
          $this->jmeno = $row['jmeno_uzivatel'];
          $this->nadpis = $row['nadpis'];
          $this->email = $row['email_uzivatel'];
          $this->text = $row['text'];
          $this->src_obrazek = $row['obrazek'];
          $this->datum = $row['datum'];
          $this->pocet_komentaru = $row['pocet_komentaru'];
          $this->id_uzivatel = $row['id_uzivatel'];
          $this->id_prispevek = $row['id_prispevek'];
          
          if($or== NULL)
          {
            $this->text = substr($this->text,0,600) . ' ......';  
          }
                                                                                                                        // TISK
          echo '
          <div class="clanek" id="'. $this->id_prispevek .'">
            <img src="img/'. $this->src_obrazek .'" alt="'. $this->src_obrazek .'" height="200" width="300">
            <div class="title">
              <h3>'. $this->datum .'</h3>   
              <h2 id="'. $this->id_uzivatel .'">'. $this->nadpis .'</h2>
               <a href="index.php?ZJ='. $this->id_uzivatel .'" title="Zobrazit uzivatel">- '. $this->jmeno .'</a> 
            </div>
            <p><a href="index.php?ZP='. $this->id_prispevek .'">' . $this->text .' </a></p>
              
            <h1>POCET KOMENTU: '. $this->pocet_komentaru .'</h1>    
          ';
           
          if($or!= NULL) PARENT::vypiskomentu($this->id_prispevek);
          
          echo '</div> ';  
      } 
    }
      
    public function vypis()
    {
      $sql = "SELECT * FROM prispevek inner join uzivatel using(id_uzivatel) ORDER BY datum DESC";
      $callback = $this->database->query($sql)->fetchAll();
      $this->vypisclanku($callback, NULL);
    }
        
    public function vypisUzivatelclanky($jm)
    {
      $sql = "SELECT * FROM prispevek inner join uzivatel using(id_uzivatel) WHERE id_uzivatel='$jm' ORDER BY datum DESC";
      $callback = $this->database->query($sql)->fetchAll();
      
      echo '<a href="index.php" class="zpet"> VRATIT ZPATKY NA UVODNI STRANKU</a>';
      
      $this->vypisclanku($callback, NULL);  
      
      echo '<a href="index.php" class="zpet"> VRATIT ZPATKY NA UVODNI STRANKU</a>'; 
    }
    
    public function VypisKonkretniho($idP)
    {
      $sql = "SELECT * FROM prispevek inner join uzivatel using(id_uzivatel) WHERE id_prispevek='$idP'";
      $callback = $this->database->query($sql)->fetchAll();
      if($callback != NULL)
      {                                                                                                           // TISK
        echo '<a href="index.php" class="zpet"> VRATIT ZPATKY NA UVODNI STRANKU</a>';
        $this->vypisclanku($callback, 123);
        
        echo '<a href="index.php" class="zpet"> VRATIT ZPATKY NA UVODNI STRANKU</a>';
        
        echo '
            <form id="PridatKoment" class="'. $idP .'" method="post">
            ';
            if(isset($_SESSION['email']) && isset($_SESSION['nick']))
            {
             echo '<input type="text" name="jmenoKoment" value="'. $_SESSION['nick'] .'" placeholder="JMÉNO" required>
                   <input type="text" name="emailKoment" value="'. $_SESSION['email'] .'" placeholder="EMAIL" required>
                   ';
            }
            else
            {
             echo '<input type="text" name="jmenoKoment"  placeholder="JMÉNO" required>
                   <input type="text" name="emailKoment"  placeholder="EMAIL" required>
                   ';
            }
              echo '<textarea name="textKoment" rows="10"></textarea>
              <input type="submit" value="Odeslat komentář" name="OdeslatKoment">
            </form>'; 
      }
    }
    
    private function VyEditClanekObrazek($filePostName, $cislo)
    {
        $slozka = "img/$cislo/";
        if (!file_exists($slozka)) mkdir("img/$cislo", 0777); 
        
        $nazev = preg_replace("/[^a-zA-Z._]+/", "", basename($_FILES[$filePostName]['name']));
        
        $slozka = $slozka . $nazev; 
        
        $type = pathinfo($slozka,PATHINFO_EXTENSION);
        if ($_FILES[$filePostName]['size'] == 0)
        {
           $this->src_soubor = "default.jpg"; 
           return (1);   
        }
        else
        {
          if ($_FILES[$filePostName]["size"] <= 2000000 && ($type == "jpg" || $type == "png" || $type == "jpeg" || $type == "gif")) 
          {
            if(move_uploaded_file($_FILES[$filePostName]['tmp_name'], $slozka)) 
            {
              $this->src_soubor = $cislo . "/" . $nazev;  
            }
            else
            {
              $this->src_soubor = "default.jpg";  
            }
            return (1);
          }
          else
          {
            echo "<script> alert('OBRAZEK NESPLNUJE MINIMALNI POZADAVKY! MAX 2Mb / JPG, PNG, JPEG, GIF'); </script>";
            return (0);
          }
        }
    }
    
    public function VytvoritClanek($nadpis,$filePostName, $text)
    {
      if(isset($_SESSION['name']) || $_SESSION['name'] == "uzivatelLogged")
      {
        $this->nadpis = $nadpis;
        $this->text = $text;
        $this->id_uzivatel = $_SESSION['id_uzivatel'];
        
        $this->src_soubor = "default.jpg";
        
        $sql = "SELECT MAX(id_prispevek) FROM prispevek";
        $callback = $this->database->query($sql)->fetch();
        $cislo = $callback['MAX(id_prispevek)'];
    
        $insert=$this->VyEditClanekObrazek($filePostName, $cislo);
        
        if($insert == 1)
        {
          $sql = "INSERT INTO prispevek 
          (id_uzivatel, nadpis, text, obrazek)
          VALUES ('$this->id_uzivatel','$this->nadpis','$this->text','$this->src_soubor')";
          $this->database->query($sql);
          echo "<script> alert('Clanek vytvoren'); </script>";
        }
      }
    }
}

?>