<?php
    if(!isset($_SESSION)) 
    {
	     session_start();
    }
    
    // DATABASE
    $PDO = new PDO('mysql:dbname=blog;host=127.0.0.1', 'root', '');
    require_once 'objekty.php';

    if(isset($_POST['NovyText']) && isset($_POST['IDkomentu']) && isset($_SESSION['id_uzivatel']))
    {
      $clanek = new prispevek($PDO);
      
      $novyText = $_POST['NovyText'];
      $ID_k = $_POST['IDkomentu'];
      $clanek->editKoment($novyText, $ID_k);
    }
    
    if(isset($_POST['IDkomentuSmazat']) && isset($_SESSION['id_uzivatel']))
    {
      $ID_p = $_POST['ZobrazitPrispevek'];
      $clanek = new prispevek($PDO);
      $ID_k = $_POST['IDkomentuSmazat'];
      $clanek->smazatKoment($ID_k, $ID_p);
    }
?>