// JavaScript Document
$( document ).ready(function() 
{   

    $.ajax(
        {
        type: "POST",
        url: 'rss.php',
        success: function(data)
        {
            var dataphp = $.parseJSON(data);
            //$("#novikycz").html("");
            
            $.each(dataphp, function(index,dataeach){
              var row = $("<li></li>");
              var a = $("<a></a>").attr("href", dataeach.link).text(dataeach.text);
              row.append(a); 
              
              $("#novinkycz").append(row);
            });
         }
    });
      
      
    $("#Registrovat").submit(function()                    
    {
        var hesloJEDNA = $("#JhesloRegistrovat").val();
        var hesloDVA = $("#DhesloRegistrovat").val();
        if(hesloJEDNA != hesloDVA)
        {
          alert("zadane hesla nesouhlasi, prosim zadejte je znova");
          
          return false;
        }
    }); 
    $("#regbutton").click(function()
    {
      $("#Registrovat").fadeToggle(1000);
      $("#Zapheslo").hide();
    });
    
    $("#forbutton").click(function()
    {
      $("#Zapheslo").fadeToggle(1000);
      $("#Registrovat").hide();
    });
    $("#ZmenitHeslo").submit(function()                    
    {
        var hesloJEDNA = $("#NoveHeslo").val();
        var hesloDVA = $("#NoveHesloZnova").val();
        if(hesloJEDNA != hesloDVA)
        {
          alert("zadane hesla nesouhlasi, prosim zadejte je znova");
          
          return false;
        }
    }); 
    
    $("#zobrazitVytvoritClanek").click(function()
    {
      $("#vytvoritClanek").fadeToggle(250);
      setTimeout(function() 
      {
      if($("#vytvoritClanek").is(":visible")) $("#zobrazitVytvoritClanek").text("zavřít");
      else $("#zobrazitVytvoritClanek").text("Vytvořit Článek"); 
      }, 300);    
    });
    
    $(".edit").click(function()
    {
      $(".EditKoment").fadeToggle(250);
      setTimeout(function() 
      {
      if($(".EditKoment").is(":visible")) $(".edit").text("SCHOVAT");
      else $(".edit").text("EDITOVAT"); 
      }, 300);  
    });    
    
    function successForAjax(data)
    {
      console.log(data);
      var scroll = $(document).scrollTop();
      location.reload();
      $(document).scrollTop(scroll);
      
    }
    
    $(".EditKoment").submit(function()
    {
        var textEdit = $(".textEditKoment", this).val();
        var id = $(this).parent().attr('id');
        $.ajax(
        {
            type: "POST",
            url: 'administrace.php',
            data: { 
                  NovyText : textEdit,
                  IDkomentu : id 
                  },
            success: function(data)
            {  
              alert ("Upraveno");
              successForAjax(data);
            }
        }); 
        return false;
    });
    
    $(".smazat").click(function()
    {
        var id = $(this).parent().attr('id');
        var con = confirm("Opravdu smazat?");
        var ZobPr = $(".clanek").attr('id');
        if(con)
        {
          $.ajax(
          {
              type: "POST",
              url: 'administrace.php',
              data: { 
                    IDkomentuSmazat : id, 
                    ZobrazitPrispevek : ZobPr
                    },
              success: function(data)
              {   
                alert ("Smazano");
                successForAjax(data);
              }
          }); 
        }
        return false;
    });
});