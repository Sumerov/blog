<?php
        $rss = new DOMDocument();
        $rss->load('http://www.novinky.cz/rss2');
        $i = 0;
        $pocet = 20;
        $data = [];
        
        foreach($rss->getElementsByTagName('item') as $item)
        {
          $link = NULL;
          $title = NULL;
          $vetev = $item->childNodes;
          foreach($item->childNodes as $vetev)
          {
            if($vetev->nodeName == 'link') $link = $vetev->nodeValue;
            else if($vetev->nodeName == 'title') $title = $vetev->nodeValue;
          }
            if($link != NULL and $title != NULL)
            {
            $data[] = [
                      "link" => $link,
                      "text" => $title
                      ];
            }
            ++$i;
            if($i == $pocet) break;  
        }
        
        echo json_encode($data);
?>