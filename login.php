<nav>
<?php
    if(isset($_POST['Prihlasit']))
    {        
      $jmeno = $_POST['jmenoLogin'];
      $heslo = $_POST['hesloLogin'];
      $jmeno = stripslashes($jmeno); 
      $heslo = stripslashes($heslo);
      
      $prihlasitUzivatel = new uzivatel($PDO, $jmeno);
      
      $result = $prihlasitUzivatel->Prihlasit($jmeno, $heslo);
      echo $result;
    } 
                                         
    if(isset($_POST['Odhlasit']))
    {
      session_destroy();
      echo "ODHLASENO";
      header( "refresh:0.1 ; url=index.php" );
    }
    
    if(isset($_POST['ZmenitHesloSubmit']) && isset($_POST['NoveHeslo']) && isset($_POST['NoveHesloZnova']))
    {
      $Jheslo = $_POST['NoveHeslo'];
      $Dheslo = $_POST['NoveHesloZnova'];
      if($Jheslo != $Dheslo)
      {
         echo "spatne heslo";
      }
      else
      {
        $jmeno = $_SESSION['nick'];
        $Jheslo = stripslashes($Jheslo);
        
        $ZmenitHeslo = new uzivatel($PDO);
        $result = $ZmenitHeslo->ZmenitHeslo($jmeno, $Jheslo);
        echo $result;
      }    
    }
    
    if(isset($_POST['Registrovat']) && isset($_POST['jmenoRegistrovat']) && isset($_POST['emailRegistrovat']))
    {
      $Jheslo = $_POST['JhesloRegistrovat'];
      $Dheslo = $_POST['DhesloRegistrovat'];
      
      if($Jheslo != $Dheslo)
      {
         echo "spatne heslo";
      }
      
      else
      {
        $jmeno = $_POST['jmenoRegistrovat'];
        $email = $_POST['emailRegistrovat'];
        $jmeno = stripslashes($jmeno); 
        $Jheslo = stripslashes($Jheslo);
        $registrovatUzivatel = new uzivatel($PDO);
        $result = $registrovatUzivatel->Registrovat($jmeno, $Jheslo, $email); 
        echo $result;
      }
    }
    
    if(isset($_POST['submitZapheslo']) && isset($_POST['emailZapheslo']))
    {
      $email = $_POST['emailZapheslo'];
      
      $zapomenuteHeslo = new uzivatel($PDO);
      $result = $zapomenuteHeslo->ZapomenuteHesloSend($email);
      echo $result;
    }
    
    if(isset($_GET['FPa']))
    {
      $kod = $_GET['FPa'];
      
      $zapomenuteHeslo = new uzivatel($PDO);
      $result = $zapomenuteHeslo->ZapomenuteHeslo($kod);
      echo $result;
    }
    
    if((!isset($_SESSION['name']) || $_SESSION['name'] != "uzivatelLogged") && !isset($_GET['FPa']))
    {   
      include_once 'templates/notlog - nav.php';
    }
    
   else if(!isset($_GET['FPa']))
   { 
    ?>
      <form id="Odhlaseni" method="post">
        <input type="submit" value="odhlasit se" name="Odhlasit">
      </form>
      
      <button id="zobrazitVytvoritClanek">Vytvořit Článek</button>
      
      <form method="post" id="vytvoritClanek" enctype="multipart/form-data">
        <textarea name="textClanek" rows="10" required></textarea> 
        <input type="text" name="nazevClanek" placeholder="NADPIS" required>
        <input type="file" name="obrazekClanek">
        <input type="submit" value="Vytvořit Článek" name="vytvoritClanek">
               
    </form>
    <?php
  }
    ?>
</nav>