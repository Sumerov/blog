<?php

    
    if(!isset($_SESSION)) 
    {
	     session_start();
    }
    
    // DATABASE
    $PDO = new PDO('mysql:dbname=blog;host=127.0.0.1', 'root', '');
    require_once 'objekty.php';
    
    include 'login.php';
    
    $clanek = new prispevek($PDO);
    
    if(isset($_POST["OdeslatKoment"]))
    {
      $jmeno = $_POST["jmenoKoment"];
      $email = $_POST["emailKoment"];
      $text = $_POST["textKoment"];
      $ID_p = $_GET['ZP'];
      
      if(isset($_SESSION['name']))
      {
        $ID_u = $_SESSION['id_uzivatel'];
      }
      else $ID_u = 0;
      $clanek->vytvoritKoment($ID_p, $ID_u, $text);
      $url= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";  
      header( "refresh:0.1 ; $url" );
    }
    
    if(!isset($_SESSION['name']) || $_SESSION['name'] != "uzivatelLogged")
    {
      include_once 'templates/notlog - article.php';
    }
    
    else
    {
      include_once 'templates/log - article.php';
    }
    
?>